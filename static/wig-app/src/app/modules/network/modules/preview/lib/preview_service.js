(function () {
    "use strict";

    define([], function () {

        return function (http, q, networkService) {
            var self = this;

            self.attrs = {
                percentage: 0,
                type: '',
                ing: false
            };

            // Index functions

            self.fReset = function () {
                self.attrs = {
                    percentage: 0,
                    type: '',
                    ing: false
                };
            };

            self.fIndexStatus = function (name, itype, iterative) {
                // Checks the status of the index building
                var qfIndexStatus = q.defer();

                http({
                    method: 'POST',
                    data: {
                        name: name,
                        itype: itype
                    },
                    url: 'index_status.json'
                })
                    .success(function (data) {
                        console.log(data);

                        self.attrs.percentage = data.percentage;
                        qfIndexStatus.resolve(data);

                        if (iterative === true && 100 !== data.percentage) {
                            setTimeout(function () {
                                self.fIndexStatus(name, itype, iterative);
                            }, 1000);
                        }
                    });

                return qfIndexStatus.promise;
            };

            self.fMakeNeighborsIndex = function (name, filterPrev) {
                var qfMakeNeighborsIndex = q.defer();

                self.attrs.type = 'neighbors';
                self.attrs.ing = true;

                self.source = '';
                if (filterPrev === 1) {
                    self.source = 'f'; // Filter
                } else {
                    self.source = 'o'; // Original
                }

                http({
                    method: 'POST',
                    data: {
                        name: name,
                        source: self.source
                    },
                    url: 'index_make_neighbors.json'
                })
                    .success(function (data) {
                        console.log(data);
                        self.attrs.ing = false;
                        qfMakeNeighborsIndex.resolve(data);
                    });

                // Start iterative status check
                setTimeout(function () {
                    self.fIndexStatus(name, 'neighbors', true);
                }, 1500);

                return qfMakeNeighborsIndex.promise;
            };

            self.fMakeLeavesIndex = function (name) {
                var qfMakeNeighborsIndex = q.defer();

                self.attrs.type = 'leaves';
                self.attrs.ing = true;

                http({
                    method: 'POST',
                    data: {
                        name: name
                    },
                    url: 'index_make_leaves.json'
                })
                    .success(function (data) {
                        console.log(data);
                        self.attrs.ing = false;
                        qfMakeNeighborsIndex.resolve(data);
                    });

                return qfMakeNeighborsIndex.promise;
            };

            // Interface functions

            self.fBackToFilters = function (name) {
                // Send the 'network' back at the FILTERING step (status == 2)
                networkService.fSetStatus(name, 2, networkService.m.possibleStates).then(function () {
                    document.location.hash = "/filter/" + name;
                });
            };

            self.fGotoViewer = function (name, what, how, type) {
                // Go to the Viewer
                var qfGotoViewer = q.defer();

                // Check input parameters
                if (-1 === ['original', 'filtered', 'index'].indexOf(what) || -1 === ['whole', 'navigate'].indexOf(how)) {
                    qfGotoViewer.resolve(null);
                    return qfGotoViewer.promise;
                }

                if (what === 'index') {

                    http({
                        method: 'POST',
                        data: {
                            name: name,
                            what: what,
                            how: how,
                            type: type,
                            attrs: {
                                what: what,
                                how: how,
                                type: type
                            }
                        },
                        url: 'preset_view.json'
                    })
                        .success(function (data) {
                            console.log(data);
                            if (0 === data.err) {
                                if ('navigate' === how) {
                                    document.location.hash = '/preview/' + name + '/chooseCenter';
                                } else if ('whole' === how) {
                                    document.location.hash = '/view/' + name;
                                } else {
                                    data.err = 5;
                                }
                            }
                            qfGotoViewer.resolve(data);
                        });

                } else {

                    http({
                        method: 'POST',
                        data: {
                            name: name,
                            what: what,
                            how: how,
                            type: type,
                            attrs: {
                                what: what,
                                how: how
                            }
                        },
                        url: 'preset_view.json'
                    })
                        .success(function (data) {
                            console.log(data);
                            if (0 === data.err) {
                                if ('navigate' === how) {
                                    document.location.hash = '/preview/' + name + '/chooseCenter';
                                } else if ('whole' === how) {
                                    document.location.hash = '/view/' + name;
                                } else {
                                    data.err = 5;
                                }
                            }
                            qfGotoViewer.resolve(data);
                        });

                }

                return qfGotoViewer.promise;
            };

        };

    });

}());