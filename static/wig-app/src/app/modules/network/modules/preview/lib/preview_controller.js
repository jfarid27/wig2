(function () {
    "use strict";

    define([], function () {

        return function (scope, model, routeParams, filterService, previewService) {
            scope.m = model;
            scope.s = previewService;

            scope.m.network_name = routeParams.name;
            scope.s.fReset();

            filterService.fOriginalStatus(scope.m.network_name).then(function (original) {
                scope.m.original = original;
                filterService.fFilteredStatus(scope.m.network_name).then(function (filter) {
                    scope.m.filter = filter;
                    if (filter.prev === 1) {
                        scope.m.what = 'filtered';
                    } else {
                        scope.m.what = 'original';
                    }
                    console.log({ready: true});
                });
            });
            
        };

    });

}());