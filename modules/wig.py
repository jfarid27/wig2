from igraph import *

class GOAmgmt:
	'''A class to manage GOs (Gene Ontologies) and GAs (Gene Annotations)'''
	def __init__(self, gofname, gafname):
		ltmp = self.make_all_dict(gofname, gafname)
		# GO dictionary
		self.dgo = ltmp[0]
		#self.dgo = self.make_go_dict(gofname)
		# GA dictionary
		self.dga = ltmp[1]
		#self.dga = self.make_ga_dict(gafname)
		# Mappings
		self.dgago = ltmp[2]
		#self.dgago = self.make_gago_dict(self.dga)
		self.dgoga = ltmp[3]
		#self.dgoga = self.make_goga_dict(self.dga)
		self.dgagon = ltmp[4]
		#self.dgagon = self.make_gagon_dict(self.dga, self.dgo)
		self.dgogan = ltmp[5]
		#self.dgogan = self.make_gogan_dict(self.dga, self.dgo)
		del ltmp
	def make_all_dict(self, gofname, gafname):
		'''Make all the dictionaries'''
		gaf = open(gafname, 'r+')
		dgo = self.make_go_dict(gofname)
		dga = {}
		dgago = {}
		dgoga = {}
		dgagon = {}
		dgogan = {}
		for row in gaf.readlines():
			if not row.startswith('!'):
				arow = row.split()
				k = arow[2]
				v = arow[3]
				if 'NOT' == v:
					v = ' '.join(arow[3:5])
				# Assign to complete Annotation Dictionary & inverse-map dictionary
				if dga.has_key(k):
					dga[k].append(arow)
					if not v in dgoga[k]:
						dgoga[k].append(v)
					if dgo.has_key(v) and not dgo[v]['name'].replace("'", "#") in dgogan[k]:
						dgogan[k].append(dgo[v]['name'].replace("'", "#"))
				else:
					dga[k] = [arow]
					dgoga[k] = [v]
					if dgo.has_key(v):
						dgogan[k] = [dgo[v]['name'].replace("'", "#")]
				# Assign to map dictionary
				if dgago.has_key(v):
					if not k in dgago[v]:
						dgago[v].append(k)
				else:
					dgago[v] = [k]
				if dgo.has_key(v):
					if dgagon.has_key(dgo[v]['name'].replace("'", "#")):
						if not k in dgagon[dgo[v]['name'].replace("'", "#")]:
							dgagon[dgo[v]['name'].replace("'", "#")].append(k)
					else:
						dgagon[dgo[v]['name'].replace("'", "#")] = [k]
		return [dgo,dga,dgago,dgoga,dgagon,dgogan]
	def make_go_dict(self, gofname):
		'''Make the GO dictionary'''
		gof = open(gofname, 'r+')
		dgo = {}
		dtemp = {}
		tempid = ''
		for row in gof.readlines():
			row = row.strip()
			if '[Term]' == row:
				# Start new d
				dtemp = {}
			elif '' == row:
				# Close d
				dgo[tempid] = dtemp
				dtemp = {}
			elif '[Typedef]' == row:
				continue
			else:
				# Prepare field
				arow = row.split(': ')
				k = arow[0]
				if len(arow) > 2:
					v = ': '.join(arow[1:(len(arow)-1)])
				else:
					v = arow[1]
				# ID field?
				if 'id' == k:
					# Save ID
					tempid = v
				else:
					# Add field
					if dtemp.has_key(arow[0]):
						if type(dtemp[arow[0]]) is str:
							dtemp[arow[0]] = [dtemp[arow[0]]]
						dtemp[arow[0]].append(arow[1])
					else:
						dtemp[arow[0]] = arow[1]
		return dgo
	def make_ga_dict(self, gafname):
		'''Make the GA dictionary'''
		gaf = open(gafname, 'r+')
		dga = {}
		for row in gaf.readlines():
			if not row.startswith('!'):
				arow = row.split()
				k = arow[2]
				v = arow[3]
				if 'NOT' == v:
					v = ' '.join(arow[3:5])
				# Assign to complete Annotation Dictionary & inverse-map dictionary
				if dga.has_key(k):
					dga[k].append(arow)
				else:
					dga[k] = [arow]
		return dga
	def make_gago_dict(self, dga):
		'''Make a dictionary mapping GAs (v) to GOs (k)'''
		dgago = {}
		for (gene,rows) in dga.items():
			for row in rows:
				k = row[3]
				if dgago.has_key(k):
					dgago[k].append(gene)
				else:
					dgago[k] = [gene]
		return dgago
	def make_goga_dict(self, dga):
		'''Make a dictionary mapping GOs (v) to GAs (k)'''
		dgoga = {}
		for (gene,rows) in dga.items():
			for row in rows:
				v = row[3]
				if dgoga.has_key(gene):
					if not v in dgoga[gene]:
						dgoga[gene].append(v)
				else:
					dgoga[gene] = [v]
		return dgoga
	def make_gagon_dict(self, dga, dgo):
		'''Make a dictionary mapping GA names (v) to GO names (k)'''
		dgagon = {}
		for (gene,rows) in dga.items():
			for row in rows:
				k = row[3]
				if dgo.has_key(k):
					k = dgo[k]['name'].replace("'", '#')
					if dgagon.has_key(k):
						dgagon[k].append(gene)
					else:
						dgagon[k] = [gene]
		return dgagon
	def make_gogan_dict(self, dga, dgo):
		'''Make a dictionary mapping GO names (v) to GA names (k)'''
		dgogan = {}
		for (gene,rows) in dga.items():
			for row in rows:
				v = row[3]
				if dgo.has_key(v):
					v = dgo[v]['name'].replace("'", '#')
					if dgogan.has_key(gene):
						if not v in dgogan[gene]:
							dgogan[gene].append(v)
					else:
						dgogan[gene] = [v]
		return dgogan

class NETwork:
	'''A class to manage networks'''
	def __init__():
		return None
