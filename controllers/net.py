from igraph import *
import itertools
import matplotlib.pyplot as plt
import mylib
import numpy as np
import operator
import os
import cPickle as pickle
import time

if request.function in ['preprocess_ajax', 'update_flag_ajax', 'go_filtered_vertices_ajax']:
    session.forget(response)

def index():
    # Prepare the response data for the layout
    response.title = 'Web Interface for Graphs'
    response.subtitle = '~ WIG'
    response.flash = 'Welcome to WIG :3'

    if not request.post_vars['max_nodes'] in ['', None]:
        db(db.opts.name == 'max_nodes').update(val=request.post_vars['max_nodes'])
        db.commit()
        response.flash = 'Settings saved! :3'

    return dict(maxn=db(db.opts.name == 'max_nodes').select().first()['val'])


def upload():
    # Prepare the response data for the layout
    response.title = 'Network Upload'
    response.subtitle = ' ~ WIGnu'

    # Form processing
    form_accepted = False
    form_errors = False
    errors = dict()
    p = []
    if 0 != len(request.post_vars):
        p = request.post_vars

        # Check NOT_EMPTY fields
        submission = True
        not_empty_fields = ['name', 'network']
        for l in not_empty_fields:
            if '' == p[l]:
                submission = False
                errors[l] = 'Can not be empty.'
        if '' != p['name']:
            if 0 != len(db(db.net.name == p['name']).select()):
                    errors['name'] = 'Name already in use, try with a new one.'
                    submission = False

        # Check values and process submission
        if submission:
                # Insert into database
                db.net.insert(name=p['name'], network=p['network'], comments=p['comments'])
                form_accepted = True
        del submission

    if 0 != len(errors): form_errors = True

    # Form response
    res = 0
    network = ''
    if form_accepted:
        res = 1
        network = p['name']
        response.flash = 'Form accepted :D'
    elif form_errors:
        response.flash = 'The form has errors! D:'
    else:
        response.flash = 'Please fill out the form.'

    del form_accepted, form_errors, p

    return dict(errors=errors, res=res, network=network)


def select():
    # Prepare the response data for the layout
    response.title = 'Network Select'
    response.subtitle = ' ~ WIGns'
    response.flash = 'Select a network! ^-^'

    rows = db(db.net).select()
    db.commit()

    return dict(rows=rows)


def visualize():
    # Prepare the response data for the layout
    response.title = 'Network Viewer'
    response.subtitle = ' ~ WIGnv'
    response.flash = 'Prepare yourself for awesomeness! *u*'

    name = request.args(0)
    n = len(db(db.net.name == name).select())

    # Read post['thr']
    if not request.post_vars['thr'] in ['', None]:
        thr = request.post_vars['thr'].split('.')
        if len(thr) == 1:
            if thr[0].isdigit():
                thr = float(thr[0])
            else:
                thr = 'none'
        elif len(thr) == 2:
            if thr[0].isdigit() and thr[1].isdigit():
                thr = float('.'.join(thr))
            else:
                thr = 'none'
        else:
            thr = 'none'

        # Apply threshold (if selected)
        if thr != 'none':
            db(db.thr.name == name).update(val=thr)
            db.commit()
            rowg = db(db.graph.name == name).select().first()
            rowt = db(db.thr.name == name).select().first()
            db.commit()
            rd = rowt['rawdata']
            rd = mylib.make_float_table(rd.strip(), "\n", "\t")
            mid = mylib.get_column(rd, 2)
            fre = mylib.get_column(rd, 4)

            # Change plot
            plt.plot(mid, fre)
            plt.xlabel('Edge weight')
            plt.ylabel('Frequency')
            plt.vlines (thr, 0, max(fre), colors=['red'], linestyles='dashed')
            plt.savefig(request.folder + 'uploads/' + name + '_eWeights.png')
            plt.clf()
            hf = open(request.folder + 'uploads/' + name + '_eWeights.png', 'rb')
            db(db.thr.name == name).update(chart=hf)
            db.commit()
            hf.close()
            os.remove(request.folder + 'uploads/' + name + '_eWeights.png')

            # Change network
            #g = Graph.Read_GraphML(request.folder + 'uploads/' + row['graph'])
            f = open(request.folder + 'uploads/' + rowg['graph'])
            g = pickle.load(f)
            f.close()
            g.delete_edges(mylib.which_le(g.es()['panda'], thr))
            g.delete_vertices(mylib.which_eq(g.degree(), 0))
            gf = open(request.folder + 'uploads/' + name + '_tmpThrG.graphml', 'w+')
            pickle.dump(g, gf)
            gf.close()
            gf = open(request.folder + 'uploads/' + name + '_tmpThrG.graphml', 'rb')
            db(db.thr.name == name).update(graph=gf, vlist="\t".join(g.vs()['name']), vcount=len(g.vs()), ecount=len(g.es()))
            db.commit()
            gf.close()
            os.remove(request.folder + 'uploads/' + name + '_tmpThrG.graphml')

            # Update gos
            gos = []
            for gol in g.vs()['go']:
                gos.extend(gol)
            gos.sort()
            gos = [[l[0], len(list(l[1]))] for l in itertools.groupby(gos)]
            gos = sorted(gos, key=operator.itemgetter(1), reverse=True)
            s = ''
            for (go,gcount) in gos:
                s += go + "\t" + str(gcount) + "\n"
            db(db.thr.name == name).update(g_onto=s)
            db.commit()

            del f, fre, g, gcount, gf, go, gos, hf, l, mid, rd, rowg, rowt, s
    else:
        row = db(db.thr.name == name).select()
        if len(row) != 0:
            row = row.first()
            thr = row['val']
            if thr == None:
                thr = 'none'
        else:
            thr = 'none'

    rowg = db(db.graph.name == name).select().first()
    rowt = db(db.thr.name == name).select().first()
    db.commit()

    if rowg is None:
        rowg = dict(pflag=0)

    ggo = mylib.make_table(rowg['g_onto'].strip(), "\n", "\t")
    if not rowt['g_onto'] in ['', None]:
        tgo = mylib.make_table(rowt['g_onto'].strip(), "\n", "\t")
        dtgo = {}
        for e in tgo:
            dtgo[e[0]] = e[1]
        for i in range(len(ggo)):
            if dtgo.has_key(ggo[i][0]):
                ggo[i].append(dtgo[ggo[i][0]])
            else:
                ggo[i].append('NA')
    else:
        for i in range(len(ggo)):
            ggo[i].append('NA')

    return dict(name=name, graph=rowg, n=n, thr=rowt, gos=ggo)


def viewer():
    # Prepare the response data for the layout
    response.title = 'Network Viewer'
    response.subtitle = ' ~ WIGnv'
    response.flash = 'Wait just a moment ^-^'
    # Get request data
    name = request.args(0)
    n = len(db(db.net.name == name).select())
    # Retrieve DAL row
    rowg = db(db.graph.name == name).select().first()
    rowt = db(db.thr.name == name).select().first()
    db.commit()

    json = []
    if rowg is None:
        rowg = dict(pflag=0)
        nodes = []
    else:
        # if a starting point is being chosen
        if not request.post_vars['start'] in ['', None]:
            sp = request.post_vars['start']
            if sp in rowt['vlist'].split("\t"):
                # Set starting point
                db(db.graph.name == name).update(pflag=4, starting_node=sp)
                db.commit()

                # ---------------- #
                # Prepare new view #
                # ---------------- #
                
                # Load threshold-graph
                f = open(request.folder + 'uploads/' + db(db.thr.name == name).select().first()['graph'])
                g = pickle.load(f)
                f.close()

                # Starting point ID
                spi = mylib.which_eq(g.vs()['name'], sp)

                # Dimensions
                max_nodes = int(db(db.opts.name == 'max_nodes').select().first()['val'])
                order = 1
                size = g.neighborhood_size(spi, order)[0]

                # Increase the neighborhood order
                while(size <= max_nodes):
                    order += 1
                    size = g.neighborhood_size(spi, order)[0]
                if size > max_nodes:
                    order -= 1
                    size = g.neighborhood_size(spi, order)[0]
                del f, max_nodes, size

                # Get neighborhood
                sg = g.subgraph(g.neighborhood(spi, order)[0])

                # Get DAL row
                row = db(db.json_graph.name == name).select().first()
                db.commit()

                if row is None:
                    db.json_graph.insert(graph_id=db(db.graph.name == name).select().first().id, name=name)
                    db.commit()

                row = db(db.json_graph.name == name).select().first()
                db.commit()

                # Prepare NODES
                gsgdvs = {}
                for n in sg.vs():
                    i = g.vs.find(name=n['name']).index
                    gsgdvs[n.index] = i

                    if not i in row['nodes_id']:
                        # Append id
                        row['nodes_id'].append(i)
                        # Append json
                        nad = n.attributes()
                        nad['id'] = 'n' + str(i)
                        pos = dict(x=nad['x'], y=nad['y'])
                        nad.pop('x')
                        nad.pop('y')
                        nad = dict(group='nodes', data=nad, position=pos)
                        nad = str(nad)
                        row['nodes_json'].append(nad)
                        json.append(nad)
                    else:
                        json.append(row['nodes_json'][row['nodes_id'].index(i)])

                # Prepare EDGES
                for e in sg.es():
                    i = g.get_eid(gsgdvs[e.source], gsgdvs[e.target])

                    if not i in row['edges_id']:
                        # Append id
                        row['edges_id'].append(i)
                        # Append json
                        ead = e.attributes()
                        ead['id'] = 'e' + str(i)
                        ead['source'] = 'n' + str(gsgdvs[e.source])
                        ead['target'] = 'n' + str(gsgdvs[e.target])
                        ead = dict(group='edges', data=ead)
                        ead = str(ead)
                        row['edges_json'].append(ead)
                        json.append(ead)
                    else:
                        json.append(row['edges_json'][row['edges_id'].index(i)])

                # Update DAL row
                db(db.json_graph.name == name).update(nodes_id=row['nodes_id'], nodes_json=row['nodes_json'], edges_id=row['edges_id'], edges_json=row['edges_json'], view_json=json)
                db.commit()
            del g, order, sp, spi
        else:
            row = db(db.graph.name == name).select().first()
            db.commit()
            # if a starting point was chosen
            if not row['starting_node'] in ['', None]:
                json = db(db.json_graph.name == name).select().first()['view_json']


        # Refresh DAL row
        row = db(db.graph.name == name).select().first()
        thr = db(db.thr.name == name).select().first()
        db.commit()

        # if a starting point is needed, load starting point list
        if row['pflag'] == 3:
            nodes = db(db.thr.name == name).select().first()['vlist'].split("\t")
            nodes.sort()
        else:
            nodes = []

    return dict(name=name, row=row, gos=sorted(mylib.make_table(thr['g_onto'].strip(), "\n", "\t"), key=operator.itemgetter(0)), n=n, nodes=nodes, j=str(json).replace("\"", "").replace("'", "\"").replace('#', "'"))


def preprocess_ajax():
    if '' != request.post_vars['name']:
        # Retrieve network name
        name = request.post_vars['name']
        # Default processing flag (pflag) value
        pflag = 0
        pstatus = ''
        ptime = 0
        pperc = 0
        if '' != request.post_vars['name'] and 1 == len(db(db.net.name == name).select()):
            rows = db(db.graph.name == name).select()
            db.commit()
            if 0 == len(rows):
                # pflag = 1 ~ processing
                pflag = 1
                pstatus += '> Starting...'
                pstart = time.time()
                db.graph.insert(net_id=db(db.net.name == name).select().first().id, name=name, pstart=pstart, pstatus=pstatus, pflag=1)
                db.commit()
                db.thr.insert(graph_id=db(db.graph.name == name).select().first().id, name=name)
                db.commit()

                #----------------------#
                # START PRE-PROCESSING #
                #----------------------#

                pstatus += "\\n> Reading file."
                db(db.graph.name == name).update(pstatus=pstatus)
                db.commit()
                t = mylib.read_table(request.folder + 'uploads/' + db(db.net.name == name).select()[0].network, "\t")

                # Read nodes
                pstatus += "\\n> Preparing nodes."
                pperc = 5
                db(db.graph.name == name).update(pstatus=pstatus, pperc=pperc)
                db.commit()

                tf_nodes = mylib.get_column(t, 0)
                ge_nodes = mylib.get_column(t, 1)

                tf_nnodes = mylib.suffixList(tf_nodes, '~TF')
                ge_nnodes = mylib.suffixList(ge_nodes, '~Gene')
                tf_unnodes = mylib.uniq(tf_nnodes)
                ge_unnodes = mylib.uniq(ge_nnodes)

                tf_unodes = mylib.uniq(tf_nodes)
                ge_unodes = mylib.uniq(ge_nodes)

                tf_nodes.sort()
                ge_nodes.sort()
                tf_nnodes.sort()
                ge_nnodes.sort()
                tf_unodes.sort()
                ge_unodes.sort()
                tf_unnodes.sort()
                ge_unnodes.sort()

                del ge_nodes, tf_nodes

                # Make graph
                pstatus += "\\n> Creating graph."
                pperc = 10
                db(db.graph.name == name).update(pstatus=pstatus, pperc=pperc)
                db.commit()
                g = Graph()
                # Add nodes
                pstatus += "\\n> Adding nodes."
                pperc = 15
                db(db.graph.name == name).update(pstatus=pstatus, pperc=pperc)
                db.commit()
                g.add_vertices(len(tf_unodes) + len(ge_unodes))
                g.vs['name'] = tf_unnodes + ge_unnodes
                g.vs['hugo'] = tf_unodes + ge_unodes
                g.vs['type'] = mylib.repl(['TF', 'Gene'], [len(tf_unodes), len(ge_unodes)])

                del ge_unodes, tf_unodes

                # Add GOs to the nodes
                pstatus += "\\n> Mining GOs."
                pperc = 20
                db(db.graph.name == name).update(pstatus=pstatus, pperc=pperc)
                db.commit()
                f = open(request.folder + 'uploads/' + db(db.pickled.name == 'dgogan').select().first()['pickle'], 'rb')
                dgogan = pickle.load(f)
                f.close()
                for v in g.vs:
                    if dgogan.has_key(v['hugo']):
                        v['go'] = dgogan[v['hugo']]
                    else:
                        v['go'] = ['unknown']

                del dgogan, f

                # Add GOs to the nodes
                pstatus += "\\n> Evaluating GO enrichment."
                pperc = 30
                db(db.graph.name == name).update(pstatus=pstatus, pperc=pperc)
                db.commit()
                gos = []
                for gol in g.vs()['go']:
                    gos.extend(gol)
                gos.sort()
                gos = [[l[0], len(list(l[1]))] for l in itertools.groupby(gos)]
                gos = sorted(gos, key=operator.itemgetter(1), reverse=True)
                s = ''
                for (go,gcount) in gos:
                    s += go + "\t" + str(gcount) + "\n"
                db(db.graph.name == name).update(g_onto=s)
                db.commit()

                del gcount, go, gos, l, s

                # Add edges
                pstatus += "\\n> Preparing edges."
                pperc = 35
                db(db.graph.name == name).update(pstatus=pstatus, pperc=pperc)
                db.commit()
                tf_inodes = mylib.vnamelTOvidl(g, tf_nnodes)
                pstatus += "\\n> Adding edges."
                pperc = 45
                db(db.graph.name == name).update(pstatus=pstatus, pperc=pperc)
                db.commit()
                ge_inodes = mylib.vnamelTOvidl(g, ge_nnodes)
                g.add_edges(mylib.edgesFromExtremities(tf_inodes, ge_inodes))
                g.es['motif'] = map(float, mylib.get_column(t, 2))
                g.es['panda'] = map(float, mylib.get_column(t, 3))

                del ge_inodes, ge_nnodes, ge_unnodes, t, tf_inodes, tf_nnodes, tf_unnodes

                # Graph layout
                pstatus += "\\n> Calculating layout."
                pperc = 60
                db(db.graph.name == name).update(pstatus=pstatus, pperc=pperc)
                db.commit()
                lay = g.layout('drl')
                pstatus += "\\n> Saving layout."
                pperc = 60
                db(db.graph.name == name).update(pstatus=pstatus, pperc=pperc)
                db.commit()
                for i in range(len(g.vs)):
                    g.vs[i]['x'] = lay[i][0]
                    g.vs[i]['y'] = lay[i][1]

                # Output graph
                pstatus += "\\n> Writing graph."
                pperc = 75
                db(db.graph.name == name).update(pstatus=pstatus, pperc=pperc)
                db.commit()
                #g.write_graphml(request.folder + 'uploads/' + name + '.graphml')
                gf = open(request.folder + 'uploads/' + name + '.graphml', 'w+')
                pickle.dump(g, gf)
                gf.close()
                pstatus += "\\n> Saving graph to database."
                pperc = 80
                db(db.graph.name == name).update(pstatus=pstatus, pperc=pperc, vcount=len(g.vs), ecount=len(g.es))
                db(db.thr.name == name).update(vcount=len(g.vs), ecount=len(g.es))
                db.commit()
                gf = open(request.folder + 'uploads/' + name + '.graphml', 'rb')
                db(db.graph.name == name).update(graph=gf)
                db.commit()
                gf.close()

                # Prepare weights histogram
                pstatus += "\\n> Preparing edge weight plot."
                pperc = 90
                db(db.graph.name == name).update(pstatus=pstatus, pperc=pperc)
                db.commit()

                # nbins calculated based on Freedman-Diaconis rule
                nbins = int((max(g.es['panda']) - min(g.es['panda'])) / (2*(np.percentile(g.es['panda'], 75) - np.percentile(g.es['panda'], 25)) / (len(g.es['panda']) ** (1.0/3))))
                h = mylib.prepHist(g.es['panda'], nbins)
                plt.plot(h['mid'], h['density'])
                plt.xlabel('Edge weight')
                plt.ylabel('Frequency')
                plt.savefig(request.folder + 'uploads/' + name + '_eWeights.png')
                plt.clf()

                s = ""
                for i in range(len(h['mid'])):
                    s += "\t".join([str(h['bin'][i][0]), str(h['bin'][i][1]), str(h['mid'][i]), str(h['count'][i]), str(h['density'][i])])
                    s += "\n"

                pstatus += "\\n> Saving plot to database."
                hf = open(request.folder + 'uploads/' + name + '_eWeights.png', 'rb')
                pperc = 95
                db(db.graph.name == name).update(pstatus=pstatus, pperc=pperc)
                db(db.thr.name == name).update(nbins=nbins, chart=hf, rawdata=s)
                db.commit()
                hf.close()

                del g
                del gf
                del h
                del hf
                del nbins
                del s

                # pflag = 2 ~ processed
                pstatus += "\\n> All done!"
                pflag = 2
                pperc = 100
                pstop=time.time()
                db(db.graph.name == name).update(pstatus=pstatus, pperc=pperc, pstop=pstop, pflag=pflag)
                db.commit()
                ptime = int(pstop - pstart)

                # Remove tmp files
                os.remove(request.folder + 'uploads/' + name + '.graphml')
                os.remove(request.folder + 'uploads/' + name + '_eWeights.png')

                #-----------------------#
                # END OF PRE-PROCESSING #
                #-----------------------#
            elif 1 == len(rows):
                row = rows[0]
                pflag = row.pflag
                pstatus = row.pstatus
                pperc = row.pperc
                pstart = row.pstart
                pstop = row.pstop
                if pstop is None:
                    ptime = int(time.time() - pstart)
                else:
                    ptime = int(pstop - pstart)
            else:
                pflag = -1
            return dict(flag=pflag, status=pstatus, perc=pperc, time=ptime, err=0)
        else:
            # pflag = 0 ~ to process & errors
            return dict(flag=pflag, status=pstatus, perc=pperc, time=ptime, err=1)
    else:
        return dict(flag=-1, status='', perc=0, time=0, err=1)


def update_flag_ajax():
    if not request.post_vars['name'] in ['', None] and not request.post_vars['flag'] in ['', None]:
        name = request.post_vars['name']
        flag = int(request.post_vars['flag'])
        db(db.graph.name == name).update(pflag=flag)
        db.commit()

        del name, flag

        return dict(err=0)
    else:
        return dict(err=1)


def go_filtered_vertices_ajax():
    if not request.post_vars['name'] in ['', None]:
        name = request.post_vars['name']
        row = db(db.thr.name == name).select()
        if len(row) != 0:
            row = row.first()

            # Retrieve lists to hide/show GO
            show_go = []
            hide_go = []
            for (k,v) in request.post_vars.items():
                if k != 'name' and k.count('-') >= 1:
                    if k.split('-')[0] == 'show' and not v in show_go:
                        show_go.append(v)
                    elif k.split('-')[0] == 'hide' and not v in hide_go:
                        hide_go.append(v)

            f = open(request.folder + 'uploads/' + row['graph'], 'rb')
            g = pickle.load(f)
            f.close()
            vs = g.vs.select(lambda v: mylib.go_filter_vs(v, show_go, hide_go))['name']

    return dict(name=name, show=show_go, hide=hide_go, vs=vs)