from igraph import *
from wig import *
import itertools
import matplotlib.pyplot as plt
import mylib
import numpy as np
import operator
import os
import cPickle as pickle
import time

# ----------------- #
# INTEGRATION STUFF #
# ----------------- #

'''Function necessary to change the web2py default delimiters {{}} to {||}'''
def render(filename, **variables):
    context = globals()
    context.update(variables)
    from gluon.template import render
    return render(filename=os.path.join(request.folder,'views',filename),
                  path=os.path.join(request.folder,'views'),
                  context=context,delimiters=('{|','|}')) 


# ----- #
# VIEWS #
# ----- #

def index():
    return render('../static/wig-app/src/index.html', toyMsg='Hello World!')


# ---------- #
# AJAX VIEWS #
# ---------- #

if request.function in ['GOcheck', 'GOmake', 'getSettings', 'setSetting', 'setSettings', 'list_networks', 'preprocess', 'status', 'upload', 'set_status', 'is_filter_init', 'filter_init', 'filter_status', 'is_filtering', 'filter_apply', 'filter_ewThr', 'index_status', 'index_make_neighbors', 'index_make_leaves']:
    session.forget(response)

# GO MGMT
def go_check():
    '''Check if everything is alright with the GO2GA dictionary'''

    # Check GO2GA
    go2ga = False
    if 1 == len(db(db.pickled.name == 'dgogan').select()):
        go2ga = True

    # Check GA2GO
    ga2go = False
    if 1 == len(db(db.pickled.name == 'dgagon').select()):
        ga2go = True

    return dict(gago=ga2go, goga=go2ga)

def go_make():
    '''Prepare the GO2GA dictionary and store it inside DAL'''
    
    d = go_check()

    if not d['gago'] or not d['goga']:
        dgoga = GOAmgmt('applications/ng_wig/static/data/go-basic.obo', 'applications/ng_wig/static/data/gene_association.goa_human')

    if not d['goga']:

        f = open('wig_dgogan','w+')
        pickle.dump(dgoga.dgogan, f)
        f.close()

        if len(db(db.pickled.name == 'dgogan').select()) == 0:
            f = open('wig_dgogan','rb')
            db.pickled.insert(name='dgogan', pickle=f)
            db.commit()
            f.close()

        os.remove('wig_dgogan')

        del f

    if not d['gago']:

        f = open('wig_dgagon','w+')
        pickle.dump(dgoga.dgagon, f)
        f.close()

        if len(db(db.pickled.name == 'dgagon').select()) == 0:
            f = open('wig_dgagon','rb')
            db.pickled.insert(name='dgagon', pickle=f)
            db.commit()
            f.close()

        os.remove('wig_dgagon')

        del f

    if not d['gago'] or not d['goga']:
        del dgoga

    return go_check()

# Settings MGMT

def get_settings():
    '''Retrieves WIG settings'''

    rows = db().select(db.opts.ALL);
    db.commit()

    d = {}
    for row in rows:
        d[row.name] = row.val

    return d

def set_setting():
    '''Sets a single WIG setting'''

    err = 1

    if not request.post_vars['name'] in ['', None] and not request.post_vars['val'] in ['', None]:
        name = request.post_vars['name']
        val = request.post_vars['val']

        rows = db(db.opts.name == name).select()
        db.commit()

        if 0 == len(rows):
            db.opts.insert(name=name, val=val)
            db.commit()
            err = 0

        elif 1 == len(rows):
            db(db.opts.name == name).update(val=val)
            db.commit()
            err = 0

        else:
            err = 2

    return dict(err=err)

def set_settings():
    '''Sets a single WIG setting'''

    lerr = 1

    if not request.post_vars['opts'] in ['', None]:
        opts = request.post_vars['opts']
        lerr = []

        for k in opts.keys():
            if k != '__proto__':
                name = k
                val = opts[k]

                rows = db(db.opts.name == name).select()
                db.commit()

                if 0 == len(rows):
                    db.opts.insert(name=name, val=val)
                    db.commit()
                    lerr.append(0)

                elif 1 == len(rows):
                    db(db.opts.name == name).update(val=val)
                    db.commit()
                    lerr.append(0)

                else:
                    lerr.append(2)

    return dict(err=lerr)

# Networks MGMT

def list_networks():
    '''Retrieves the list of networks present in the DAL'''

    rows = db().select(db.network.ALL)

    l = []
    for row in rows:
        l.append(dict(name=row.name, status=row.status, note=row.notes))

    return dict(list=l);

def upload():
    '''Check the FormData passed by POST and, if everything is fine, inserts the network in the DAL.'''

    # Form processing
    form_accepted = False
    form_errors = False
    errors = dict()

    post_vars = request.post_vars

    if 0 != len(post_vars):

        # Check NOT_EMPTY fields
        submissionOK = True

        for required_field in ['name', 'network']:
            if '' == post_vars[required_field]:
                errors[required_field] = 'Can not be empty.'
                form_errors = True
                submissionOK = False

        if '' != post_vars['name']:
            if 0 != len(db(db.network.name == post_vars['name']).select()):
                    errors['name'] = 'Name already in use, try with a new one.'
                    form_errors = True
                    submissionOK = False

        # Check values and process submission
        if submissionOK:
            db.network.insert(name=post_vars['name'], fnetwork=post_vars['network'], notes=post_vars['note'])
            db.commit()
            form_accepted = True

        del submissionOK

    if 0 != len(errors): form_errors = True

    # Form response [msg]
    response = 0
    name = ''

    if form_accepted:
        response = 1
        name = post_vars['name']
        msg = 'Form accepted :D'
    elif form_errors:
        response = 2
        msg = 'The form has errors! D:'
    else:
        msg = 'Please fill out the form.'

    del form_accepted, form_errors, post_vars

    return dict(d=dict(r=response, msg=msg, name=name, err=errors))

def preprocess():
    '''Perform network preprocessing'''

    if '' != request.post_vars['name']:

        # Retrieve network name
        name = request.post_vars['name']

        # Default processing flag (pflag) value
        net_status = -1
        status_label = ''
        duration = 0
        preprocess_perc = 0

        # If the network is available
        rows = db(db.network.name == name).select()
        if '' != request.post_vars['name'] and 1 == len(rows):

            # Is it being pre-processed?
            if rows.first().status == '0':

                # Update network status
                net_status = '1'
                db(db.network.name == name).update(status=net_status)
                db.commit()

                # Check if there is something in the graph table
                rows = db(db.graph.network_name == name).select()
                db.commit()

                # Remove it
                if 0 != len(rows):
                    db(db.graph.network_name == name).delete()
                    db.commit()

                #Starting
                status_label += '> Starting...'
                start_time = time.time()
                db.preprocessing.insert(
                    network_id=db(db.network.name == name).select().first().id,
                    network_name=name,
                    start_time=start_time,
                    status_label=status_label
                )
                db.graph.insert(
                    network_id=db(db.network.name == name).select().first().id,
                    network_name=name,
                )
                db.commit()

                #----------------------#
                # START PRE-PROCESSING #
                #----------------------#

                # Read file
                status_label += "\\n> Reading file."
                db(db.preprocessing.network_name == name).update(status_label=status_label)
                db.commit()

                t = mylib.read_table(request.folder + 'uploads/' + db(db.network.name == name).select()[0].fnetwork, "\t")

                # Prepare nodes
                status_label += "\\n> Preparing nodes."
                percentage = 5
                db(db.preprocessing.network_name == name).update(status_label=status_label, percentage=percentage)
                db.commit()

                tf_nodes = mylib.get_column(t, 0)
                ge_nodes = mylib.get_column(t, 1)

                tf_nnodes = mylib.suffixList(tf_nodes, '~TF')
                ge_nnodes = mylib.suffixList(ge_nodes, '~Gene')
                tf_unnodes = mylib.uniq(tf_nnodes)
                ge_unnodes = mylib.uniq(ge_nnodes)

                tf_unodes = mylib.uniq(tf_nodes)
                ge_unodes = mylib.uniq(ge_nodes)

                tf_unodes.sort()
                ge_unodes.sort()
                tf_unnodes.sort()
                ge_unnodes.sort()

                del ge_nodes, tf_nodes

                # Make graph
                status_label += "\\n> Creating graph."
                percentage = 10
                db(db.preprocessing.network_name == name).update(status_label=status_label, percentage=percentage)
                db.commit()

                g = Graph()

                # Add nodes
                status_label += "\\n> Adding nodes."
                percentage = 15
                db(db.preprocessing.network_name == name).update(status_label=status_label, percentage=percentage)
                db.commit()

                g.add_vertices(len(tf_unodes) + len(ge_unodes))
                g.vs['name'] = tf_unnodes + ge_unnodes
                g.vs['hugo'] = tf_unodes + ge_unodes
                g.vs['type'] = mylib.repl(['TF', 'Gene'], [len(tf_unodes), len(ge_unodes)])

                del ge_unodes, tf_unodes

                # Add GOs to the nodes
                status_label += "\\n> Mining GOs."
                percentage = 20
                db(db.preprocessing.network_name == name).update(status_label=status_label, percentage=percentage)
                db.commit()

                f = open(request.folder + 'uploads/' + db(db.pickled.name == 'dgogan').select().first()['pickle'], 'rb')
                dgogan = pickle.load(f)
                f.close()

                for v in g.vs:
                    if dgogan.has_key(v['hugo']):
                        v['go'] = dgogan[v['hugo']]
                    else:
                        v['go'] = ['unknown']

                del dgogan, f

                # Prepare GO tables
                status_label += "\\n> Evaluating GO enrichment."
                percentage = 30
                db(db.preprocessing.network_name == name).update(status_label=status_label, percentage=percentage)
                db.commit()

                gos = []
                for gol in g.vs()['go']:
                    gos.extend(gol)
                gos.sort()

                gos = [[l[0], len(list(l[1]))] for l in itertools.groupby(gos)]

                gos = sorted(gos, key=operator.itemgetter(1), reverse=True)

                s = ''
                for (go,gcount) in gos:
                    s += go + "\t" + str(gcount) + "\n"

                db(db.graph.network_name == name).update(gene_onto=s)
                db.commit()

                del gcount, go, gos, l, s

                # Prepare edges
                status_label += "\\n> Preparing edges."
                percentage = 35
                db(db.preprocessing.network_name == name).update(status_label=status_label, percentage=percentage)
                db.commit()

                tf_inodes = mylib.vnamelTOvidl(g, tf_nnodes)
                ge_inodes = mylib.vnamelTOvidl(g, ge_nnodes)

                # Add edges
                status_label += "\\n> Adding edges."
                percentage = 45
                db(db.preprocessing.network_name == name).update(status_label=status_label, percentage=percentage)
                db.commit()

                g.add_edges(mylib.edgesFromExtremities(tf_inodes, ge_inodes))
                g.es['motif'] = map(float, mylib.get_column(t, 2))
                g.es['panda'] = map(float, mylib.get_column(t, 3))

                del ge_inodes, ge_nnodes, ge_unnodes, t, tf_inodes, tf_nnodes, tf_unnodes

                # Output graph
                status_label += "\\n> Writing graph."
                percentage = 75
                db(db.preprocessing.network_name == name).update(status_label=status_label, percentage=percentage)
                db.commit()

                #g.write_graphml(request.folder + 'uploads/' + name + '.graphml')
                gf = open(request.folder + 'uploads/' + name + '.graphml', 'w+')
                pickle.dump(g, gf)
                gf.close()

                # Save graph
                status_label += "\\n> Saving graph to database."
                percentage = 80
                db(db.preprocessing.network_name == name).update(status_label=status_label, percentage=percentage)
                db(db.graph.network_name == name).update(v_count=len(g.vs), e_count=len(g.es))
                db.commit()

                gf = open(request.folder + 'uploads/' + name + '.graphml', 'rb')
                db(db.graph.network_name == name).update(fgraph=gf)
                db.commit()
                gf.close()

                # Prepare weights histogram
                status_label += "\\n> Preparing edge weight plot."
                percentage = 90
                db(db.preprocessing.network_name == name).update(status_label=status_label, percentage=percentage)
                db.commit()

                # nbins calculated based on Freedman-Diaconis rule
                nbins = int((max(g.es['panda']) - min(g.es['panda'])) / (2*(np.percentile(g.es['panda'], 75) - np.percentile(g.es['panda'], 25)) / (len(g.es['panda']) ** (1.0/3))))
                h = mylib.prepHist(g.es['panda'], nbins)
                plt.plot(h['mid'], h['density'])
                plt.xlabel('Edge weight')
                plt.ylabel('Frequency')
                plt.savefig(request.folder + 'uploads/' + name + '_eWeights.png')
                plt.clf()

                s = ""
                for i in range(len(h['mid'])):
                    s += "\t".join([str(h['bin'][i][0]), str(h['bin'][i][1]), str(h['mid'][i]), str(h['count'][i]), str(h['density'][i])])
                    s += "\n"

                status_label += "\\n> Saving plot to database."
                hf = open(request.folder + 'uploads/' + name + '_eWeights.png', 'rb')

                percentage = 95
                db(db.preprocessing.network_name == name).update(status_label=status_label, percentage=percentage)
                db(db.graph.network_name == name).update(ew_nbins=nbins, ew_chart=hf, ew_rawdata=s)
                db.commit()
                hf.close()

                del g, gf, h, hf, nbins, s
                os.remove(request.folder + 'uploads/' + name + '.graphml')
                os.remove(request.folder + 'uploads/' + name + '_eWeights.png')

                # Preprocessed
                status_label += "\\n> All done!"
                net_status = '2'
                percentage = 100
                stop_time = time.time()
                db(db.preprocessing.network_name == name).update(status_label=status_label, percentage=percentage, stop_time=stop_time)
                db(db.network.name == name).update(status=net_status)
                db.commit()

                duration = int(stop_time - start_time)

                #-----------------------#
                # END OF PRE-PROCESSING #
                #-----------------------#

                return dict(status=net_status, status_label=status_label, percentage=percentage, duration=duration, err=0)
            else:
                # Being pre-processed
                # Check if there is something in the graph table
                rows = db(db.preprocessing.network_name == name).select()
                db.commit()

                if 1 == len(rows):
                    # Return status
                    row = rows.first()

                    if row.stop_time in ['', None]:
                        stop_time = time.time()
                    else:
                        stop_time = row.stop_time
                    duration = int(stop_time - row.start_time)

                    rd = dict(status=db(db.network.name == name).select().first().status, status_label=row.status_label, percentage=row.percentage, duration=duration, err=0)
                    db.commit()

                    return rd
                else:
                    # Error in graph table
                    return dict(status=net_status, status_label=status_label, percentage=percentage, duration=duration, err=3)
        else:
            # Network not available
            return dict(status=net_status, status_label=status_label, percentage=percentage, duration=duration, err=2)
    else:
        # No network requested
        return dict(status=-1, status_label='', percentage=0, duration=0, err=1)

def status():
    '''Retrieve network status'''

    # Default preprocessing values
    net_status = -1
    status_label = ''
    percentage = 0
    duration = 0
    err = 1

    v_count = 0
    e_count = 0
    g_onto = ''
    ew_chart = ''
    ew_rawdata = ''
    ew_nbins = 0

    if not request.post_vars['name'] in ['', None]:

        # Retrieve network name
        name = request.post_vars['name']

        # If the network is available
        rows = db(db.network.name == name).select()
        if 1 == len(rows):

            # Is it being pre-processed?
            if rows.first().status == '0':

                net_status = '0'
                status_label = 'Not yet started.'

                return dict(status=net_status, status_label=status_label, percentage=percentage, duration=duration, err=0)
            else:
                # Being pre-processed
                # Check if there is something in the graph table
                rows = db(db.preprocessing.network_name == name).select()
                db.commit()

                if 1 == len(rows):
                    # Return status
                    row = rows.first()

                    if row.stop_time in ['', None]:
                        stop_time = time.time()
                    else:
                        stop_time = row.stop_time
                    duration = int(stop_time - row.start_time)

                    rd = dict(status=db(db.network.name == name).select().first().status, status_label=row.status_label, percentage=row.percentage, duration=duration, err=0)
                    db.commit()

                    row = db(db.graph.network_name == name).select().first()
                    rd['e_count'] = row['e_count']
                    rd['v_count'] = row['v_count']
                    rd['gene_onto'] = mylib.make_table(row['gene_onto'], "\n", "\t")
                    rd['ew_chart'] = row['ew_chart']
                    rd['ew_rawdata'] = row['ew_rawdata']
                    rd['ew_nbins'] = row['ew_nbins']

                    return rd
                else:
                    # Error in graph table
                    return dict(status=net_status, status_label=status_label, percentage=percentage, duration=duration, e_count=e_count, v_count=v_count, g_onto=g_onto, ew_chart=ew_chart, ew_rawdata=ew_rawdata, ew_nbins=ew_nbins, err=3)
        else:
            # Network not available
            return dict(status=net_status, status_label=status_label, percentage=percentage, duration=duration, e_count=e_count, v_count=v_count, g_onto=g_onto, ew_chart=ew_chart, ew_rawdata=ew_rawdata, ew_nbins=ew_nbins, err=2)
    else:
        # No network requested
        return dict(status=net_status, status_label=status_label, percentage=percentage, duration=duration, e_count=e_count, v_count=v_count, g_onto=g_onto, ew_chart=ew_chart, ew_rawdata=ew_rawdata, ew_nbins=ew_nbins, err=err)

def set_status():
    err = -1

    if not request.post_vars['name'] in ['', None] and not request.post_vars['status'] in ['', None]:
        name = request.post_vars['name']
        status = request.post_vars['status']

        rows = db(db.network.name == name).select()
        if 0 != len(rows):
            db(db.network.name == name).update(status=status)
            db.commit()

            err = 0
        else:
            err = 1

    return dict(err=err)

# Network Filters MGMT

def is_filter_init():
    '''Checks if a filter was initiated for the given network'''

    if not request.post_vars['name'] in ['', None]:
        name = request.post_vars['name']
        rows = db(db.filters.network_name == name).select()

        if 0 == len(rows):
            # Not yet initialized
            err = 0

        else:
            # Already initialized
            err = 2

        del name, rows

    else:
        # No network specified
        err = 1

    return dict(err=err)

def filter_init():
    '''Initializes filter MGMT for the given network'''

    if not request.post_vars['name'] in ['', None]:
        name = request.post_vars['name'];
        rows = db(db.filters.network_name == name).select()

        if 0 == len(rows):
            rows = db(db.network.name == name).select()

            if 0 != len(rows):
                db.filtered_graph.insert(network_id=db(db.network.name == name).select().first(), network_name=name)
                db.filters.insert(network_id=db(db.network.name == name).select().first(), network_name=name, filter_try=db(db.filtered_graph.network_name == name).select().first())

                db.commit()
                err = 0

            else:
                # Specified network does not exist
                err = 3

        else:
            # Already initialized
            err = 2

        del name, rows

    else:
        # No network specified
        err = 1

    return dict(err=err)

def filter_status():
    '''Retrieve filtered network status'''

    # Default preprocessing values
    v_count = None
    e_count = None
    gene_onto = None
    ew_chart = None
    ew_rawdata = None
    ew_nbins = None
    fattr = dict()
    fstatus = -1
    previous = 0

    # No network requested
    err = -1

    if not request.post_vars['name'] in ['', None]:
        name = request.post_vars['name']

        # If the network is available
        rows = db(db.network.name == name).select()
        if 1 == len(rows):

            # If it is being filtered
            rows = db(db.filters.network_name == name).select()
            if 1 == len(rows):
                row = rows.first()
                if not row.filter_try in ['', None]:
                    if not row.filter_try.filter_type in ['', None]:
                        f = row.filter_try
                        v_count = f.v_count
                        e_count = f.e_count
                        gene_onto = f.gene_onto
                        ew_chart = f.ew_chart
                        ew_rawdata = f.ew_rawdata
                        ew_nbins = f.ew_nbins
                        fattr = f.fattr
                        fstatus = f.fstatus
                        err = 0

                        del f

                    else:
                        # Check for previous filtered graphs
                        if 0 != len(row.graph_list):
                            f = db(db.filtered_graph.id == row.graph_list[-1]).select().first()
                            v_count = f.v_count
                            e_count = f.e_count
                            gene_onto = f.gene_onto
                            ew_chart = f.ew_chart
                            ew_rawdata = f.ew_rawdata
                            ew_nbins = f.ew_nbins
                            fattr = f.fattr
                            fstatus = f.fstatus
                            err = 0
                            previous = 1

                            del f
                        else:
                            # No current filter_try.type specified
                            fstatus = 0
                            err = 5

                else:
                    # No current filter_try
                    err = 4

                del row

            else:
                # No filterMGMT
                err = 3

        else:
            # Network not available
            err = 2

        del name, rows
    else:
        err = 1
    
    if not None == gene_onto and str == type(gene_onto):
        gene_onto = mylib.make_table(gene_onto, "\n", "\t")

    return dict(e_count=e_count, v_count=v_count, gene_onto=gene_onto, ew_chart=ew_chart, ew_rawdata=ew_rawdata, ew_nbins=ew_nbins, fattr=fattr, fstatus=fstatus, prev=previous, err=err)

def is_filtering():
    '''Checks if the given network has a current filter_try'''

    if not request.post_vars['name'] in ['', None]:
        name = request.post_vars['name']
        rows = db(db.filters.network_name == name).select()

        if 0 == len(rows):
            # Not yet initialized
            err = 0

        else:
            if not rows.first().filter_try in ['', None]:
                # Is filtering
                err = 3

            else:
                # Is not filtering
                err = 2

        del name, rows

    else:
        # No network specified
        err = 1

    return dict(err=err)

def filter_apply():
    '''Applies the current filter_try'''

    if not request.post_vars['name'] in ['', None]:
        name = request.post_vars['name']
        rows = db(db.filters.network_name == name).select()

        if 0 == len(rows):
            # Not yet initialized
            return dict(err=0)

        else:
            row = rows.first()

            if not row.filter_try in ['', None]:
                if not row.filter_try.filter_type in ['', None]:
                    
                    # Update ew_chart
                    f = open(request.folder + 'uploads/' + row.filter_try.fgraph)
                    g = pickle.load(f)
                    f.close()

                    nbins = int((max(g.es['panda']) - min(g.es['panda'])) / (2*(np.percentile(g.es['panda'], 75) - np.percentile(g.es['panda'], 25)) / (len(g.es['panda']) ** (1.0/3))))
                    h = mylib.prepHist(g.es['panda'], nbins)
                    plt.plot(h['mid'], h['density'])
                    plt.xlabel('Edge weight')
                    plt.ylabel('Frequency')
                    plt.savefig(request.folder + 'uploads/' + name + '_eWeights.png')
                    plt.clf()

                    s = ""
                    for i in range(len(h['mid'])):
                        s += "\t".join([str(h['bin'][i][0]), str(h['bin'][i][1]), str(h['mid'][i]), str(h['count'][i]), str(h['density'][i])])
                        s += "\n"

                    hf = open(request.folder + 'uploads/' + name + '_eWeights.png', 'rb')
                    db(db.filtered_graph.id == row.filter_try.id).update(ew_chart=hf, ew_rawdata=s, ew_nbins=nbins)
                    db.commit()

                    hf.close()
                    del f, g, h, hf, s
                    os.remove(request.folder + 'uploads/' + name + '_eWeights.png')

                    # Assign filtered_graph to filters and prepare new filter_try

                    graph_list = row.graph_list
                    if graph_list == None:
                        graph_list = []
                    graph_list.append(row.filter_try)

                    filter_list = row.filter_list
                    if filter_list == None:
                        filter_list = []
                    filter_list.append(row.filter_try.filter_type)

                    db.filtered_graph.insert(network_id=db(db.network.name == name).select().first(), network_name=name)
                    db(db.filters.network_name == name).update(graph_list=graph_list, filter_list=filter_list, filter_try=db((db.filtered_graph.network_name == name) & (db.filtered_graph.filter_type == '')).select().first())
                    db.commit()

                    del filter_list, graph_list

                    return dict(err=0)

                else:
                    # No real filter_try available
                    return dict(err=3)
            else:
                # Is not filtering
                return dict(err=2)

    else:
        # No network specified
        return dict(err=1)

def filter_reset():
    err = -1

    if not request.post_vars['name'] in ['', None]:
        name = request.post_vars['name']

        rows =  db(db.network.name == name).select()
        db.commit()
        if 0 != len(rows):
            db(db.filtered_graph.network_name == name).delete()
            db(db.filters.network_name == name).delete()
            db(db.index_network.network_name == name).delete()
            db(db.views.network_name == name).delete()
            db.commit()

            err = 0

        else:
            err = 2

        del name, rows
    else:
        err = 1

    return dict(err=err)

def filter_reset_try():
    err = -1

    if not request.post_vars['name'] in ['', None]:
        name = request.post_vars['name']

        rows = db(db.filters.network_name == name).select()
        if 0 != len(rows):
            row = rows.first().filter_try
            db(db.filtered_graph.id == row.id).update(filter_type='', fgraph=None, v_list=None, v_count=None, e_count=None, gene_onto=None, ew_chart=None, ew_rawdata=None, ew_nbins=None, fattr=dict(), fstatus=0)
            db.commit()

            err = 0

            del row
        else:
            err = 2

        del name, rows
    else:
        err = 1

    return dict(err=err)

def filter_ewThr():

    err = -1

    # Read post['thr']
    if not request.post_vars['thr'] in ['', None] and not request.post_vars['name'] in ['', None]:
        name = request.post_vars['name']

        if db(db.filters.network_name == name).select().first().filter_try.fstatus == 0:

            thr = request.post_vars['thr']
            if not type(thr) is float:
                thr = mylib.string2float(thr)

            # Apply threshold (if selected)
            rows = db(db.filters.network_name == name).select()
            if None != thr and 0 != len(rows):
                row = rows.first()

                # Save filter type
                db(db.filtered_graph.id == row.filter_try.id).update(filter_type='ewThr', fattr=dict(thr=thr), fstatus=-1)
                db.commit()

                # Get network
                if 0 == len(row.graph_list):
                    # original network
                    rowg = db(db.graph.network_name == name).select().first()
                    db.commit()
                else:
                    # filtered network
                    rowg = db(db.filtered_graph.id == row.graph_list[-1]).select().first()
                    db.commit()
                    
                f = open(request.folder + 'uploads/' + rowg.fgraph)
                g = pickle.load(f)
                f.close()

                # Change plot
                nbins = int((max(g.es['panda']) - min(g.es['panda'])) / (2*(np.percentile(g.es['panda'], 75) - np.percentile(g.es['panda'], 25)) / (len(g.es['panda']) ** (1.0/3))))
                h = mylib.prepHist(g.es['panda'], nbins)
                plt.plot(h['mid'], h['density'])
                plt.xlabel('Edge weight')
                plt.ylabel('Frequency')
                plt.vlines (thr, 0, max(h['density']), colors=['red'], linestyles='dashed')
                plt.savefig(request.folder + 'uploads/' + name + '_eWeights.png')
                plt.clf()

                s = ""
                for i in range(len(h['mid'])):
                    s += "\t".join([str(h['bin'][i][0]), str(h['bin'][i][1]), str(h['mid'][i]), str(h['count'][i]), str(h['density'][i])])
                    s += "\n"

                hf = open(request.folder + 'uploads/' + name + '_eWeights.png', 'rb')
                db(db.filtered_graph.id == row.filter_try.id).update(ew_chart=hf, ew_rawdata=s, ew_nbins=nbins)
                db.commit()

                hf.close()
                os.remove(request.folder + 'uploads/' + name + '_eWeights.png')

                # Change network
                g.delete_edges(mylib.which_le(g.es()['panda'], thr))
                g.delete_vertices(mylib.which_eq(g.degree(), 0))

                gf = open(request.folder + 'uploads/' + name + '_tmpThrG.graphml', 'w+')
                pickle.dump(g, gf)
                gf.close()

                gf = open(request.folder + 'uploads/' + name + '_tmpThrG.graphml', 'rb')
                db(db.filtered_graph.id == row.filter_try.id).update(fgraph=gf, v_list=g.vs()['name'], v_count=len(g.vs()), e_count=len(g.es()))
                db.commit()

                gf.close()
                os.remove(request.folder + 'uploads/' + name + '_tmpThrG.graphml')

                # Update gos
                gos = []

                for gol in g.vs()['go']:
                   gos.extend(gol)
                gos.sort()

                gos = [[l[0], len(list(l[1]))] for l in itertools.groupby(gos)]
                gos = sorted(gos, key=operator.itemgetter(1), reverse=True)

                s = ''
                for (go,gcount) in gos:
                   s += go + "\t" + str(gcount) + "\n"

                db(db.filtered_graph.id == row.filter_try.id).update(gene_onto=s, fstatus=0)
                db.commit()

                del f, g, gcount, gf, go, gos, hf, l, rowg, s

                err = 0

            else:
                err = 2

        else:
            err = 1

    return dict(err=err, thr=request.post_vars['thr'], name=request.post_vars['name'])

def filter_GOwl():
    err = -1

    if not request.post_vars['golist'] in ['', None] and not request.post_vars['name'] in ['', None]:
        name = request.post_vars['name']

        if db(db.filters.network_name == name).select().first().filter_try.fstatus == 0:
            golist = request.post_vars['golist']

            # Apply filter
            rows = db(db.filters.network_name == name).select()
            if 0 != len(rows):
                row = rows.first()

                # Save filter type
                db(db.filtered_graph.id == row.filter_try.id).update(filter_type='GOwl', fattr=dict(golist=golist), fstatus=-1)
                db.commit()

                # Get network
                if 0 == len(row.graph_list):
                    # original network
                    rowg = db(db.graph.network_name == name).select().first()
                    db.commit()
                else:
                    # filtered network
                    rowg = db(db.filtered_graph.id == row.graph_list[-1]).select().first()
                    db.commit()
                    
                f = open(request.folder + 'uploads/' + rowg.fgraph)
                g = pickle.load(f)
                f.close()

                # Change network
                g.delete_vertices(g.vs.select(lambda v: mylib.go_filter_vs(v, [], golist))['name'])

                gf = open(request.folder + 'uploads/' + name + '_tmpGOwl.graphml', 'w+')
                pickle.dump(g, gf)
                gf.close()

                gf = open(request.folder + 'uploads/' + name + '_tmpGOwl.graphml', 'rb')
                db(db.filtered_graph.id == row.filter_try.id).update(fgraph=gf, v_list=g.vs()['name'], v_count=len(g.vs()), e_count=len(g.es()))
                db.commit()

                gf.close()
                os.remove(request.folder + 'uploads/' + name + '_tmpGOwl.graphml')

                # Update gos
                gos = []

                for gol in g.vs()['go']:
                   gos.extend(gol)
                gos.sort()

                gos = [[l[0], len(list(l[1]))] for l in itertools.groupby(gos)]
                gos = sorted(gos, key=operator.itemgetter(1), reverse=True)

                s = ''
                for (go,gcount) in gos:
                   s += go + "\t" + str(gcount) + "\n"

                db(db.filtered_graph.id == row.filter_try.id).update(gene_onto=s)
                db.commit()

                # nbins calculated based on Freedman-Diaconis rule
                nbins = int((max(g.es['panda']) - min(g.es['panda'])) / (2*(np.percentile(g.es['panda'], 75) - np.percentile(g.es['panda'], 25)) / (len(g.es['panda']) ** (1.0/3))))
                h = mylib.prepHist(g.es['panda'], nbins)
                plt.plot(h['mid'], h['density'])
                plt.xlabel('Edge weight')
                plt.ylabel('Frequency')
                plt.savefig(request.folder + 'uploads/' + name + '_eWeights.png')
                plt.clf()

                s = ""
                for i in range(len(h['mid'])):
                    s += "\t".join([str(h['bin'][i][0]), str(h['bin'][i][1]), str(h['mid'][i]), str(h['count'][i]), str(h['density'][i])])
                    s += "\n"

                hf = open(request.folder + 'uploads/' + name + '_eWeights.png', 'rb')
                db(db.filtered_graph.id == row.filter_try.id).update(ew_chart=hf, ew_rawdata=s, ew_nbins=nbins, fstatus=0)
                db.commit()
                hf.close()

                del f, g, gcount, gf, go, gos, h, hf, l, nbins, rowg, s
                os.remove(request.folder + 'uploads/' + name + '_eWeights.png')

                err = 0
            else:
                err = 3
        else:
            err = 2
    else:
        err = 1

    return dict(err=err, pv=request.post_vars)

# IndexNetwork Builders

def index_status():
    '''Retrieves the status of the current indexNetwork building'''
    err = -1
    perc = 0

    if not request.post_vars['name'] in ['', None] and not request.post_vars['itype'] in ['', None]:
        name = request.post_vars['name']
        itype = request.post_vars['itype']

        row = db((db.index_network.network_name == name) & (db.index_network.itype == itype)).select().first()
        db.commit()

        perc = row.percentage
        err = 0

    else:
        err = 1

    return dict(err=err, percentage=perc)

def index_make_neighbors():
    '''Build a neighborhood-index network'''
    err = -1

    if not request.post_vars['name'] in ['', None] and not request.post_vars['source'] in ['', None]:
        name = request.post_vars['name']
        source = request.post_vars['source']

        if source in ['f', 'o']:

            rows = db((db.index_network.network_name == name) & (db.index_network.itype == 'neighbors')).select()
            db.commit()
            if 0 == len(rows):
                db.index_network.insert(network_id=db(db.network.name == name).select().first().id, network_name=name, itype='neighbors')
                db.commit()
                rows = db((db.index_network.network_name == name) & (db.index_network.itype == 'neighbors')).select()
                db.commit()
            else:
                # Already built
                err = 3
                return dict(err=err)
                # !!! Delete when filters are changed
                
            row = rows.first()

            if source == 'o':
                # Original
                g = db(db.graph.network_name == name).select().first().fgraph

            elif source == 'f':
                # Filtered
                g = db(db.filtered_graph.id == db(db.filters.network_name == name).select().first().graph_list[-1]).select().first().fgraph

            # Retrieve network
            f = open(request.folder + 'uploads/' + g)
            g = pickle.load(f)
            f.close()

            # New network
            h = Graph()

            # Focus on TF node_type
            v_type = g.vs.select(type_eq='TF')
            # Add vertices
            for v in v_type:
                h.add_vertex(v['name'], size=g.degree(v.index))

            # Prepare dictionary to recieve the edges
            de = {}
            for v1 in v_type:
                de[v1.index] = {}
                for v2 in v_type:
                    de[v1.index][v2.index] = 0
            # Fill up the dictionary
            v_ntype = g.vs.select(type_ne='TF')
            for i in range(len(v_ntype)):
                con = v_ntype[i]
                neighbors = g.neighbors(con.index)
                for v1 in neighbors:
                    for v2 in neighbors:
                        if v1 != v2:
                            de[v1][v2] += 1
                if 0 == i % 100:
                    db(db.index_network.id == row.id).update(percentage=(i * 100) / len(v_ntype))
                    db.commit()

            # Add edges
            for source in de.keys():
                for target in de[source].keys():
                        if de[source][target]/2 != 0 and len(h.es.select(_source_eq=target,_target_eq=source)) == 0:
                            h.add_edge(source, target, weight=de[source][target]/2)

            hf = open(request.folder + 'uploads/' + name + '.graphml', 'w+')
            pickle.dump(h, hf)
            hf.close()
            hf = open(request.folder + 'uploads/' + name + '.graphml', 'rb')
            db(db.index_network.id == row.id).update(fgraph=hf, v_count=len(h.vs), e_count=len(h.es), percentage=100)
            db.commit()
            hf.close()

            del de, f, g, h, hf, v_type, v_ntype
            os.remove(request.folder + 'uploads/' + name + '.graphml')

            err = 0

        else:
            err = 2

    else:
        err = 1

    return dict(err=err)

def index_make_leaves():
    err = -1
    return dict(err=err)

# Viewer

def preset_view():
    '''Prepares the viewer'''
    err = -1
    itype=0

    if not request.post_vars['name'] in ['', None] and  not request.post_vars['what'] in ['', None] and  not request.post_vars['how'] in ['', None]:
        name = request.post_vars['name']
        what = request.post_vars['what']
        how = request.post_vars['how']

        if not request.post_vars['what'] in ['original', 'filtered', 'index'] and  not request.post_vars['how'] in ['whole', 'navigate']:
            err = 2
            return dict(err=err)

        rows =  db(db.views.network_name == name).select()
        db.commit()

        if 0 == len(rows):
            db.views.insert(network_id=db(db.network.name == name).select().first(), network_name=name)
            row = db(db.views.network_name == name).select().first()
            db.commit()
        else:
            row = rows.first()

        if not request.post_vars['type'] in ['', None]:
            itype = request.post_vars['type']

            if 'index' == what:
                if itype in ['neighbors', 'leaves']:
                    db(db.views.id == row.id).update(attrs=dict(what=what, how=how, type=itype))
                    err = 0
                else:
                    err = 3
            else:
                db(db.views.id == row.id).update(attrs=dict(what=what, how=how))
                err = 0
        else:
            if 'index' != what:
                db(db.views.id == row.id).update(attrs=dict(what=what, how=how))
                err = 0
            else:
                err = 4

        if 0 == err and 'navigate' != how:
            db(db.network.name == name).update(status=4)
            db.commit()
    else:
        err = 1

    return dict(err=err, pv=request.post_vars, i=itype)

def view_status():
    err = -1
    status = dict()

    if not request.post_vars['name'] in ['', None]:
        name = request.post_vars['name']

        rows = db(db.views.network_name == name).select()
        if 0 != len(rows):
            status = rows.first().attrs
            err = 0
        else:
            err = 1

    return dict(status=status, err=err)

def view_preload():
    err = -1

    if not request.post_vars['name'] in ['', None] and not request.post_vars['status'] in ['', None]:
        name = request.post_vars['name']
        status = request.post_vars['status']

        if status['what'] in ['index', 'original', 'filtered'] and status['how'] in ['whole', 'navigate']:
            if status['what'] == 'index':
                if status['type'] in ['leaves', 'neighbors']:
                    rows = db((db.index_network.network_name == name) & (db.index_network.itype == status['type'])).select()
                    if 0 != len(rows):
                        row = rows.first()
                        if status['how'] == 'whole':

                            # Get graph
                            f = open(request.folder + 'uploads/' + row.fgraph, 'rb')
                            g = pickle.load(f)
                            f.close()

                            lv = []
                            for v in g.vs:
                                temp = v.attributes()
                                temp['id'] = 'n' + str(v.index)
                                lv.append(temp)

                            lav = {}
                            for a in g.vs[0].attribute_names():
                                lav[a] = g.vs[a]

                            le = []
                            for e in g.es:
                                temp = e.attributes()
                                temp['source'] = 'n' + str(e.source)
                                temp['target'] = 'n' + str(e.target)
                                le.append(temp)

                            lae = {}
                            for a in g.es[0].attribute_names():
                                lae[a] = g.es[a]

                            del f, g, temp

                            return dict(err=0, es=le, vs=lv, eas=lae, vas=lav)

                            err = 0
                        elif status['how'] == 'navigate':
                            err = 0
                    else:
                        err = 3
                else:
                    err = 2
            elif status['what'] == 'original':
                err = 0
            elif status['what'] == 'filtered':
                err = 0
        else:
            err = 1

    return dict(err=err)

# ------ #
# BASICS #
# ------ #

@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)
